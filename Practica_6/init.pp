class  helloworld {
  notify { 'hello, world!': }

  file { '/home/savanzado2022':
    ensure => directory,
    owner => 'root', 
  }

  file { '/home/savanzado2022/docker-compose.yml':
    owner => 'root', 
    group => 'root', 
    mode => '0644',
    source => 'puppet:///modules/helloworld/docker-compose.yml',
  }
}