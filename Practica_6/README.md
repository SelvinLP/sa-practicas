# Practica 6

## Integrantes
Integrantes de desarrollo
| Nombre | Carnet |
|:--------------|:-------------:|
| Selvin Lisandro Aragón Pérez | 201701133 |
| Erwin Alejandro García Barrera | 201700801 |  

## Enunciado
1. [Practica 6](https://drive.google.com/file/d/1OYCRIhKabsfkjv-6J88Wht2xQEMhH2w6/view?usp=sharing)

## Herramientas
Para esta practica se utilizo las siguientes herramientas
 
### Jenkins
***
Jenkins es un servidor de automatización open source escrito en Java. Está basado en el proyecto Hudson y es, dependiendo de la visión, un fork del proyecto o simplemente un cambio de nombre.

### SonarQube
***
SonarQube es una plataforma para evaluar código fuente. Es software libre y usa diversas herramientas de análisis estático de código fuente como Checkstyle, PMD o FindBugs para obtener métricas que pueden ayudar a mejorar la calidad del código de un programa.

### Docker hub
***
Docker toma las imágenes mediante las cual provisiona nuestros contenedores en donde nuestra aplicación reside.

### Docker
***
Es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos

### Angular
***
Angular es un framework para aplicaciones web desarrollado en TypeScript, de código abierto, mantenido por Google, que se utiliza para crear y mantener aplicaciones web de una sola página

### Puppet
***
Es una herramienta de gestión de la configuración de código abierto. Está escrito en Ruby y fue liberado bajo la Licencia Pública General de GNU hasta la versión 2.7.0 y después bajo la licencia Apache 2.0. 

### Comandos
0. Version
```
Version de node 16 en jenkins
```
1. Instalar dependencias en el nodo maestro
```
//Docker sin sudo
sudo groupadd docker
sudo gpasswd -a $USER docker

sudo apt-get install wget
//jenkins
//sudo apt install openjdk-11-jre
sudo wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt update
sudo apt install jenkins
sudo systemctl start jenkins
sudo systemctl status jenkins
sudo cat /var/lib/jenkins/secrets/initialAdminPassword

//Eliminar sudo jenkins
sudo su 
visudo -f /etc/sudoers
jenkins ALL= NOPASSWD: ALL

//Error de docker en jenkins
sudo usermod -a -G docker jenkins

sudo nano /etc/hosts
10.182.0.8 puppetmaster puppet
10.182.0.9 puppetclient1
10.182.0.10 puppetclient2
 

wget https://apt.puppetlabs.com/puppet7-release-focal.deb
sudo dpkg -i puppet7-release-focal.deb
sudo apt-get update -y
sudo apt-get install puppetserver -y

nano /etc/default/puppetserver
//Cambiar los g2 a g1 
JAVA_ARGS="-Xms1g -Xmx1g -Djruby.logger.class=com.puppetlabs.jruby_utils.jruby.Slf4jLogger"

sudo systemctl start puppetserver
sudo systemctl enable puppetserver
sudo systemctl status puppetserver


//Despues de configurar los nodos 
sudo /opt/puppetlabs/bin/puppetserver ca list
sudo /opt/puppetlabs/bin/puppetserver ca sign --all
sudo /opt/puppetlabs/bin/puppet agent --t
```

2. Instalar dependencias en el nodo cliente
```
sudo nano /etc/hosts
10.182.0.8 puppetmaster puppet
10.182.0.9 puppetclient1
10.182.0.10 puppetclient2

wget https://apt.puppetlabs.com/puppet7-release-focal.deb
sudo dpkg -i puppet7-release-focal.deb
sudo apt-get update -y
sudo apt-get install puppet-agent -y
sudo nano /etc/puppetlabs/puppet/puppet.conf
sudo systemctl start puppet
sudo systemctl enable puppet
sudo systemctl status puppet

//Copiar del error y colocarlo en el puppet.conf como parte del servidor
sa-practicas-master.us-west4-b.c.practicassa-341818.internal
//agregar en puppet.conf
runinterval = 120s
```

3. Comprobar cambios

```
//Aplicar cambios
sudo /opt/puppetlabs/bin/puppet agent -t
//Ver cambios en los nodos
cat /etc/motd

```

### Links
Instalación
https://noviello.it/es/como-instalar-puppet-agent-en-ubuntu-20-04-lts/
https://howtoforge.es/instalar-puppet-master-y-agente-en-ubuntu-20-04/
https://www.youtube.com/watch?v=2uS-YHCvCE8