class  helloworld::motd { 
  file { '/etc/motd': 
  owner => 'root', 
  group => 'root', 
  mode => '0644', 
  content => "Hello, Selvin, I'm a puppet follower\n", } 


  exec {'list_docker_containers':
    command => "/bin/bash -c 'docker ps -a'",
    notify  => Exec['stop_docker_containers'],
  }

  exec {'stop_docker_containers':
    command => "/bin/bash -c 'docker stop $(docker ps -aq) -f || true'",
    notify  => Exec['clear_docker_containers'],
  }

  exec {'clear_docker_containers':
    command => "/bin/bash -c 'docker rm $(docker ps -aq) -f || true'",
    notify  => Exec['clear_docker_images'],
  }

  exec {'clear_docker_images':
    command => "/bin/bash -c 'docker rmi $(docker images -q) || true'",
    notify  => Exec['pull_docker_images'],
  }

  exec {'pull_docker_images':
    command => "/bin/bash -c 'docker-compose pull'",
    cwd => "/home/savanzado2022/",
    notify  => Exec['up_docker_images'],
  }

  exec {'up_docker_images':
    command => "/bin/bash -c 'docker-compose up -d --no-build'",
    cwd => "/home/savanzado2022/",
  }
  }