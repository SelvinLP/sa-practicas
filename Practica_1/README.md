# Practica 1

## Integrantes
Integrantes de desarrollo
| Nombre | Carnet |
|:--------------|:-------------:|
| Selvin Lisandro Aragón Pérez | 201701133 | 
| Erwin Alejandro García Barrera | 201700801 |  

## Enunciado
1. [Practica 1](https://drive.google.com/file/d/19neyfkO8kwW7lEivjp9x20ZMsPovOehe/view?usp=sharing)

## Herramientas
Para esta practica se utilizo las siguientes herramientas

### Node
***
Es un entorno en tiempo de ejecución multiplataforma, de código abierto, para la capa del servidor basado en el lenguaje de programación JavaScript, asíncrono, con E/S de datos en una arquitectura orientada a eventos y basado en el motor V8 de Google

### JWT
***
Es un estándar qué está dentro del documento RFC 7519. En el mismo se define un mecanismo para poder propagar entre dos partes, y de forma segura, la identidad de un determinado usuario, además con una serie de claims o privilegios.

## Endpoints
Para comunicarnos con el sevidor en necesario utilizar los siguientes endpoints

Iniciar Sesion al sistema _localhost:3000/login_
``` json

{
        "username":"Selvin",
        "password":"pass9876",
        "rol":"Cliente"
}
```

Cerrar Sesion al sistema _localhost:3000/logout_
``` json
{
        "username":"Selvin",
        "password":"pass9876",
        "rol":"Cliente"
}
```

Ver pedidos del cliente _localhost:3001/mispedidos_
``` json
 {
        "status": "Success",
        "pedidos": [],
}
```


Solicitar nuevo pedido al restaurante _localhost:3001/newpedido_
``` json
 {
        "restaurante": "PizzaSA",
        "noOrden": "5",
        "estado": "Enviada",
        "repartidor": "Luis",
        "cliente": "Selvin",
        "total":50,
        "descripcion": "Lorem ipsum"
}
```

Ver pedidos del cliente en camino _localhost:3001/encamino_
``` json
{
        "status": "Success",
        "pedidos": [],
}
```

Ver ordenes desde el restaurante _localhost:3002/misordenes_
``` json
{
        "status": "Success",
        "pedidos": [],
}
```

Ver ordenes de entrega del repartidor _localhost:3003/misentregas_
``` json
{
        "status": "Success",
        "entregas": [],
}
```

Actualizar entrega del repartidor _localhost:3003/updateEntrega_
``` json
{
        "noOrden": 1231,
        "estado": "Completado",

}
```

