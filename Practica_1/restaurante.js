const express = require('express')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const fs = require('fs')
const app = express()
var http = require('http');
const { json } = require('body-parser');
require('dotenv').config()
app.use(express.json())

//--------------------------FUNCIONES
callback = function(response) {
  var str = '';

  response.on('data', function (chunk) {
    str += chunk;
  });

  response.on('end', function () {
    pedidos = JSON.parse(str)
  });
}

//-------------------------------OBTENER LOS PEDIDOS
http.request('http://localhost:3001/allPedidos', callback).end();

function writeLog(log){
    let date = new Date()
    let str = 'Fecha: ' + date + ' Log: ' + log + '\n' 
    fs.appendFile('log.txt',str,function(err){
        if(err) return console.log(err)
    })
}

//-------------------------------VARIABLES
let usuarioOnline = {
    nombre: '',
    rol: ''
}

let pedidos = []

//-----------------------------FUNCIONES
function checkRol(){
    if(usuarioOnline.rol == 'Restaurante'){
        return true
    }
    return false
}

function validarToken(req, res, next){
    const header = req.headers['authorization']
    const token = header && header.split(' ')[1]
    if(token == null) return res.sendStatus(401)

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user)=>{
        if(err) return res.sendStatus(403)
        usuarioOnline.nombre = user.name
        usuarioOnline.rol = user.rol
        //console.log(usuarioOnline)
        req.user = user
        next()
    })
}

//--------------------------VER MIS ORDENES COMO RESTAURANTE
app.get('/misordenes', validarToken, (req,res)=>{
    writeLog('Ver las ordenes de ' + req.body.username)
    mispedidos = pedidos.filter(p => p.restaurante === usuarioOnline.nombre)
    if(mispedidos == null){
        return res.status(402).send('No hay pedidos')
    }else{
        if(checkRol()){
        res.json(mispedidos)
        }else{
            res.status(402).send("Solo restaurantes pueden ver sus ordenes")
        }
    }
})

//-----------------------------ACTUALIZAR PEDIDO
app.put('/updatePedido',validarToken, (req,res)=>{
    writeLog('Actualizar pedido' + req.body.username)
    let updated = false
    pedidos.forEach(p => {
        if(p.noOrden == req.body.noOrden){
            p.estado = req.body.estado
            p.repartidor = req.body.repartidor
            updated = true
            res.status(202).send("Pedido actualizado")
        }
    });
    if(!updated){
        res.status(202).send("El pedido no pudo actualizarse")
    }else{
        console.log(pedidos)
    }
})


//-------------------------------------OBTENER TODOS LOS PEDIDOS
app.get('/allPedidos', (req,res) =>{
    res.json(pedidos)
})

//--------------------------------------INICIAR EL SERVICIO
app.listen(process.env.PORTRESTAURANTE, process.env.HOST, ()=>{
    console.log(`Restaurant server running on http://${process.env.HOST}:${process.env.PORTRESTAURANTE}`)
})