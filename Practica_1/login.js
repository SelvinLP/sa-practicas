const express = require('express')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const fs = require('fs')
const app = express()

require('dotenv').config()
app.use(express.json())

/****************************************************
                    Variables
*****************************************************/ 
var users = [
    {
        username:'Alejandro',
        password:'pass1234',
        rol:'Cliente'
    },
    {
        username:'HamburguesasSA',
        password:'pass4321',
        rol:'Restaurante'
    },
    {
        username:'Jhon',
        password:'pass6789',
        rol:'Repartidor'
    },
    {
        username:'Luis',
        password:'pass6788',
        rol:'Repartidor'
    },
    {
        username:'Selvin',
        password:'pass9876',
        rol:'Cliente'
    }
]

let reTokens = []

function generarToken(user){
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '15m'})
}

function writeLog(log){
    let date = new Date()
    let str = 'Fecha: ' + date + ' Log: ' + log + '\n' 
    fs.appendFile('log.txt',str,function(err){
        if(err) return console.log(err)
    })
}
//------------------------------------------INICIAR SESION
app.post('/login',async (req, res)=>{
    writeLog('Inicio de sesion ' + req.body.username)
    
    //Authenticate User
    const user = users.find(user => user.username === req.body.username && user.rol === req.body.rol)
    if(user == null){
        writeLog('Usuario no existe ' + req.body.username)
        return res.status(400).send('Usuario no existe')
    }

    try{
        //if(await bcrypt.compare(req.body.password, user.password)){
        if(req.body.password === user.password){
            //JWT
            const username = req.body.username
            const rol = req.body.rol
            const user = {name: username, rol: rol}
            const accessToken = generarToken(user)
            const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET)
            reTokens.push(refreshToken)

            res.json({
            accessToken: accessToken,
            refreshToken: refreshToken
            })
        }else{
            writeLog('Error: no permitido ' + req.body.username)
            res.send('Not allowed')
        }
    }catch{
        res.status(500).send()
    }
})

//------------------------------OBTENER UN NUEVO TOKEN
app.post('/token', (req, res)=>{
    const reToken = req.body.token
    //console.log(reTokens)
    if(reToken == null) return res.sendStatus(401)
    if(!reTokens.includes(reToken)) return res.sendStatus(403)
    jwt.verify(reToken, process.env.REFRESH_TOKEN_SECRET, (err, user) =>{
        if(err) return res.sendStatus(403)
        const accessToken = generarToken({name: user.name})
        res.json({accessToken: accessToken})
    })
})

//--------------------------------CERRAR SESION
app.delete('/logout', (req, res)=>{
    writeLog('Cierre de sesion' + req.body.username)
    console.log(reTokens)
    reTokens = reTokens.filter(token => token !== req.body.token)
    
    res.status(204).send("Sesion finalizada")
})

//---------------------------------OBTENER TODOS LOS USUARIOS
app.get('/users', (req, res)=>{
    res.json(users)
})

//-----------------------------------INICIO DEL SERVICIO
app.listen(process.env.PORTLOGIN, process.env.HOST, ()=>{
    console.log(`Login server running on http://${process.env.HOST}:${process.env.PORTLOGIN}`)
})