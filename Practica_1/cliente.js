require('dotenv').config()
const express = require('express')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const app = express()
app.use(express.json())
const fs = require('fs')

//--------------------------------------VARIABLES
let usuarioOnline = {
    nombre: '',
    rol: ''
}

let pedidos = [
    {
        restaurante: 'HamburguesasSA',
        noOrden: '1',
        estado: 'Preparacion',
        repartidor: '',
        cliente: 'Alejandro',
        total:50,
        descripcion: 'Lorem ipsum'
    },
    {
        restaurante: 'HamburguesasSA',
        noOrden: '2',
        estado: 'entregada',
        repartidor: 'Luis',
        cliente: 'Alejandro',
        total:50,
        descripcion: 'Lorem ipsum'
    },
    {
        restaurante: 'PizzaSA',
        noOrden: '5',
        estado: 'Enviada',
        repartidor: 'Luis',
        cliente: 'Selvin',
        total:50,
        descripcion: 'Lorem ipsum'
    }
]

//----------------------------------FUNCIONES
function validarToken(req, res, next){
    const header = req.headers['authorization']
    const token = header && header.split(' ')[1]
    if(token == null) return res.sendStatus(401)

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user)=>{
        if(err){ return res.sendStatus(403)}
        //console.log(user)
        usuarioOnline.nombre = user.name
        usuarioOnline.rol = user.rol
        //console.log(usuarioOnline)
        req.user = user
        next()
    })
}

function checkRol(){
    if(usuarioOnline.rol == 'Cliente'){
        return true
    }
    return false
}

function writeLog(log){
    let date = new Date()
    let str = 'Fecha: ' + date + ' Log: ' + log + '\n' 
    fs.appendFile('log.txt',str,function(err){
        if(err) return console.log(err)
    })
}
//--------------------------------VERIFICAR ESTADO DEL PEDIDO RESTAURANTE
app.post('/mispedidos', validarToken, (req,res)=>{
    writeLog('Ver las pedidos de ' + req.body.username)
    mispedidos = pedidos.filter(p => p.cliente === usuarioOnline.nombre && p.restaurante === req.body.restaurante)
    if(mispedidos == null || mispedidos.length == 0){
        return res.status(402).send('No hay pedidos')
    }else{
        if(checkRol()){
        res.json(mispedidos)
        }else{
            res.status(402).send("Solo clientes pueden ver sus pedidos")
        }
    }
})

//--------------------------------VERIFICAR ESTADO DEL PEDIDO AL REPARTIDOR
app.post('/encamino', validarToken, (req,res)=>{
    writeLog('Ver los pedidos enviados de ' + req.body.username)
    misordenes = pedidos.filter(p => p.cliente === usuarioOnline.nombre && p.repartidor === req.body.repartidor)
    if(misordenes == null){
        return res.status(402).send('No hay pedidos en camino')
    }else{
        if(checkRol()){
        res.json(misordenes)
        }else{
            res.status(402).send("Solo clientes pueden ver sus pedidos en camino")
        }
    }
})

//---------------------------NUEVO PEDIDO
app.post('/newpedido', validarToken, (req,res) =>{
    writeLog('Nuevo pedido de ' + req.body.username)
    pedido = {
        restaurate: req.body.restaurante,
        noOrden: pedidos.length + 1,
        estado: 'Recibida',
        repartidor: '',
        cliente: usuarioOnline.nombre,
        total:req.body.total,
        descripcion: req.body.descripcion
    }
    pedidos.push(pedido)
    res.status(200).send("Pedido recibido exitosamente")
})

//----------------------------------------OBTENER TODOS LOS PEDIDOS
app.get('/allPedidos', (req,res) =>{
    res.json(pedidos)
})

//----------------------------------------INICIAR EL SERVICIO
app.listen(process.env.PORTCLIENTE, process.env.HOST, ()=>{
    console.log(`Client server running on http://${process.env.HOST}:${process.env.PORTCLIENTE}`)
})