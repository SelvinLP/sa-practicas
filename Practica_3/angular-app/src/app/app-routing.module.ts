import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepracticaComponent } from './practica/homepractica/homepractica.component';

const routes: Routes = [
  { path: '', component: HomepracticaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
