# Practica 3

## Integrantes
Integrantes de desarrollo
| Nombre | Carnet |
|:--------------|:-------------:|
| Selvin Lisandro Aragón Pérez | 201701133 |
| Erwin Alejandro García Barrera | 201700801 |  

## Enunciado
1. [Practica 3](https://drive.google.com/file/d/1o8hP2JhTBQ6TFE1fyB91yii5VIk1VSJE/view?usp=sharing)

## Herramientas
Para esta practica se utilizo las siguientes herramientas

### Jenkins
***
Jenkins es un servidor de automatización open source escrito en Java. Está basado en el proyecto Hudson y es, dependiendo de la visión, un fork del proyecto o simplemente un cambio de nombre.

### Docker hub
***
Docker toma las imágenes mediante las cual provisiona nuestros contenedores en donde nuestra aplicación reside.

### Docker
***
Es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos

### Angular
***
Angular es un framework para aplicaciones web desarrollado en TypeScript, de código abierto, mantenido por Google, que se utiliza para crear y mantener aplicaciones web de una sola página

## Comandos
Los comando a utilizar son los siguientes

Instalar y ejecutar Jenkins
```
docker build -t jenkins-ch .
docker run -u root -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock --restart unless-stopped jenkins-ch:latest
```

Buscar la contraseña en _D:\Programs\Jenkins/secrets/initialAdminPassword_ o ver la constraseña al crear el contenedor

Crear una imagen 
```
docker login -u "$DOCKER_USER" -p "$DOCKER_PASSWORD"
```

Loguearse en docker hub
```
docker login -u "$DOCKER_USER" -p "$DOCKER_PASSWORD"
```

Publicar la imagen en docker hub
```
docker image push selvinlp/pareja12:1.0
```

Descargar una imagen en docker hub
```
docker pull selvinlp/pareja12:1.0
```

Ejecutar la imagen
```
docker run -d --name practica2 -p 80:80 selvinlp/pareja12:1.0
```