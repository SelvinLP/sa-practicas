# Practica 7

## Integrantes
Integrantes de desarrollo
| Nombre | Carnet |
|:--------------|:-------------:|
| Selvin Lisandro Aragón Pérez | 201701133 |
| Erwin Alejandro García Barrera | 201700801 |  

## Enunciado
1. [Practica 7](https://drive.google.com/file/d/1An7XUavciymuhWt52ciARaAl5WPBzmOy/view?usp=sharing)

## Herramientas
Para esta practica se utilizo las siguientes herramientas
 
### Jenkins
***
Jenkins es un servidor de automatización open source escrito en Java. Está basado en el proyecto Hudson y es, dependiendo de la visión, un fork del proyecto o simplemente un cambio de nombre.

### SonarQube
***
SonarQube es una plataforma para evaluar código fuente. Es software libre y usa diversas herramientas de análisis estático de código fuente como Checkstyle, PMD o FindBugs para obtener métricas que pueden ayudar a mejorar la calidad del código de un programa.

### Docker hub
***
Docker toma las imágenes mediante las cual provisiona nuestros contenedores en donde nuestra aplicación reside.

### Docker
***
Es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos

### Angular
***
Angular es un framework para aplicaciones web desarrollado en TypeScript, de código abierto, mantenido por Google, que se utiliza para crear y mantener aplicaciones web de una sola página

### Terraform
***
Es un software de infraestructura como código desarrollado por HashiCorp. Permite a los usuarios definir y configurar la infraestructura de un centro de datos en un lenguaje de alto nivel

Infraestructura como codigo

## Instalar terraform

Pagina oficial - https://www.terraform.io/


## Jeninks

Instalar el plugin - manage plugin - buscar - terraform - install without restart
global tools configurations - add terraform - name terraform - install directory

## Configuracion pipeline


## Configuracion GCP
Id practicassa-341818

