terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.5.0"
    }
    docker = {
            source = "kreuzwerker/docker"
            version = "2.16.0"
    }
  }
}

provider "google" {
  credentials = file("practicassa-341818-3586cf4f098a.json")

  project = "practicassa-341818"
  region  = "us-west4"
  zone    = "us-west4-b"
}

resource "google_compute_subnetwork" "network_subnet" {
  name = "terraform-subnetwork"
  ip_cidr_range = "10.10.10.0/24"
  network = google_compute_network.vpc_network.name
  region = "us-west4"
}

# Allow http
resource "google_compute_firewall" "allow-http" {
  name    = "allow-http"
  network = google_compute_network.vpc_network.name
  allow {
    protocol = "tcp"
    ports    = ["80","8080","8081","8082","90","9000"]
  }
  
  source_ranges = ["0.0.0.0/0"]
  target_tags = ["http"] 
}

resource "google_compute_network" "vpc_network" {
  name = "terraform-network"
  auto_create_subnetworks = "false"
  routing_mode = "GLOBAL"
}

# allow ssh
resource "google_compute_firewall" "allow-ssh" {
  name    = "allow-ssh"
  network = google_compute_network.vpc_network.name
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  source_ranges = ["0.0.0.0/0"]
  target_tags = ["ssh"]
}

data "template_file" "linux-metadata" {
template = <<EOF
sudo apt-get update; 
sudo apt-get install -y docker.io;
sudo docker pull selvinlp/sa-p12:latest;
docker run -d --name practica7 -p 8081:80 selvinlp/sa-p12:latest;
EOF
}

resource "google_compute_instance" "vm_instance" {
  name         = "terraform-instance"
  machine_type = "f1-micro"
  zone         = "us-west4-b"
  tags = ["ssh", "http"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }


metadata_startup_script = data.template_file.linux-metadata.rendered


  network_interface {
    network = google_compute_network.vpc_network.name
    subnetwork = google_compute_subnetwork.network_subnet.name
    access_config {
    }
  }
}