# Practica 5

## Integrantes
Integrantes de desarrollo
| Nombre | Carnet |
|:--------------|:-------------:|
| Selvin Lisandro Aragón Pérez | 201701133 |
| Erwin Alejandro García Barrera | 201700801 |  

## Enunciado
1. [Practica 5](https://drive.google.com/file/d/1jf2gzf0KpojuLPQzImIdMMkRVQt6iM1r/view?usp=sharing)

## Herramientas
Para esta practica se utilizo las siguientes herramientas
 
### Jenkins
***
Jenkins es un servidor de automatización open source escrito en Java. Está basado en el proyecto Hudson y es, dependiendo de la visión, un fork del proyecto o simplemente un cambio de nombre.

### SonarQube
***
SonarQube es una plataforma para evaluar código fuente. Es software libre y usa diversas herramientas de análisis estático de código fuente como Checkstyle, PMD o FindBugs para obtener métricas que pueden ayudar a mejorar la calidad del código de un programa.

### Docker hub
***
Docker toma las imágenes mediante las cual provisiona nuestros contenedores en donde nuestra aplicación reside.

### Docker
***
Es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos

### Angular
***
Angular es un framework para aplicaciones web desarrollado en TypeScript, de código abierto, mantenido por Google, que se utiliza para crear y mantener aplicaciones web de una sola página

### Comandos
1. Instalar ansible en el nodo de control
sudo apt-get install python
sudo apt install ansible
2. inventario de maquinas a configurar
cd etc/ansible/hosts
3. SSH

docker run -d --name practica7 -p 8081:80 selvinlp/pareja12:1.0
docker run -d --name practica2 -p 8082:80 selvinlp/pareja12:1.0

Servidor Central -- 34.125.81.65
sa-practicas

Servidor  Produccion -- 34.136.229.149
sproduccion

Servidor pruebas -- 35.193.149.236
spruebas

ssh-keygen -t rsa
cd ~/.ssh
cat id_rsa.pub //copiar contenido 
pegar en llaves SSH del servidor central

4. Tareas
ansible-playbook playbook.yml

5. Ejecutar 