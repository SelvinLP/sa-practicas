import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { HomepracticaComponent } from './homepractica.component';

describe('HomepracticaComponent', () => {
  let component: HomepracticaComponent;
  let fixture: ComponentFixture<HomepracticaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomepracticaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepracticaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Test for name', () => {    
    fixture = TestBed.createComponent(HomepracticaComponent);   
    component = fixture.componentInstance;    
    const name = fixture.debugElement.query(By.css('h4')).nativeElement;
    expect(name.innerHTML).toBe('Practica 6');
  });
});
