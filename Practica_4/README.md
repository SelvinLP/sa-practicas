# Practica 4

## Integrantes
Integrantes de desarrollo
| Nombre | Carnet |
|:--------------|:-------------:|
| Selvin Lisandro Aragón Pérez | 201701133 |
| Erwin Alejandro García Barrera | 201700801 |  

## Enunciado
1. [Practica 4](https://drive.google.com/file/d/1L06KAeZpMJN62shV5zplAyNa_OOstz5L/view?usp=sharing)

## Herramientas
Para esta practica se utilizo las siguientes herramientas

### Jenkins
***
Jenkins es un servidor de automatización open source escrito en Java. Está basado en el proyecto Hudson y es, dependiendo de la visión, un fork del proyecto o simplemente un cambio de nombre.

### SonarQube
***
SonarQube es una plataforma para evaluar código fuente. Es software libre y usa diversas herramientas de análisis estático de código fuente como Checkstyle, PMD o FindBugs para obtener métricas que pueden ayudar a mejorar la calidad del código de un programa.

### Docker hub
***
Docker toma las imágenes mediante las cual provisiona nuestros contenedores en donde nuestra aplicación reside.

### Docker
***
Es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos

### Angular
***
Angular es un framework para aplicaciones web desarrollado en TypeScript, de código abierto, mantenido por Google, que se utiliza para crear y mantener aplicaciones web de una sola página

## Comandos
Los comando a utilizar son los siguientes

Configuraciones de Sonarqueb
```
npm install --save-dev sonar-scanner
"sonar": "sonar-scanner" #Agregar a package.json
{ type: 'lcov' } #Agregar en reporters

sudo vi /etc/sysctl.conf
vm.max_map_count=262144
fs.file-max=65536
:wq! #Para salir del editor

sudo sysctl -p
sudo apt-get update

```

Instalar y ejecutar Jenkins
```
docker build -t jenkins-ch .
docker run -u root -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock --restart unless-stopped jenkins-ch:latest
```

Buscar la contraseña en _D:\Programs\Jenkins/secrets/initialAdminPassword_ o ver la constraseña al crear el contenedor

Crear una imagen 
```
docker login -u "$DOCKER_USER" -p "$DOCKER_PASSWORD"
```

Loguearse en docker hub
```
docker login -u "$DOCKER_USER" -p "$DOCKER_PASSWORD"
```

Publicar la imagen en docker hub
```
docker image push selvinlp/pareja12:1.0
```

Descargar una imagen en docker hub
```
docker pull selvinlp/pareja12:1.0
```

Ejecutar la imagen
```
docker run -d --name practica2 -p 80:80 selvinlp/pareja12:1.0
```

## Links
https://tomgregory.com/sonarqube-quality-gates-in-jenkins-build-pipeline/

https://www.coachdevops.com/2021/12/install-sonarqube-using-docker-install.html

https://medium.com/polyglots-blog/angular-fitbit-jenkins-sonarqube-829cc6201469

https://www.youtube.com/watch?v=4AEW-yR_Biw