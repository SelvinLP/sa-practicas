docker build -t selvinlp/pareja12:1.0 .
docker login -u "$DOCKER_USER" -p "$DOCKER_PASSWORD"
docker image push selvinlp/pareja12:1.0
docker pull selvinlp/pareja12:1.0
docker run -d --name practica2 -p 80:80 selvinlp/pareja12:1.0