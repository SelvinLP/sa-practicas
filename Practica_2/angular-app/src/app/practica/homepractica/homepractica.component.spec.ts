import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepracticaComponent } from './homepractica.component';

describe('HomepracticaComponent', () => {
  let component: HomepracticaComponent;
  let fixture: ComponentFixture<HomepracticaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomepracticaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepracticaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
