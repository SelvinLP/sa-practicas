# Practica 2

## Integrantes
Integrantes de desarrollo
| Nombre | Carnet |
|:--------------|:-------------:|
| Selvin Lisandro Aragón Pérez | 201701133 |
| Erwin Alejandro García Barrera | 201700801 |  

## Enunciado
1. [Practica 2](https://drive.google.com/file/d/117X8Qjnz7DtLlT_HURaP5cmVK4Augkho/view?usp=sharing)

## Herramientas
Para esta practica se utilizo las siguientes herramientas

### Docker hub
***
Docker toma las imágenes mediante las cual provisiona nuestros contenedores en donde nuestra aplicación reside.

### Docker
***
Es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos

## Comandos
Para publicar y utilizar imagenes en docker hub se utilizan los siguientes comandos

Crear una imagen 
```
docker build -t selvinlp/pareja12:1.0 .

```

Loguearse en docker hub
```
docker login -u "$DOCKER_USER" -p "$DOCKER_PASSWORD"
```

Publicar la imagen en docker hub
```
docker image push selvinlp/pareja12:1.0
```

Descargar una imagen en docker hub
```
docker pull selvinlp/pareja12:1.0
```

Ejecutar la imagen
```
docker run -d --name practica2 -p 80:80 selvinlp/pareja12:1.0
```
